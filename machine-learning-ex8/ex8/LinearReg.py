import numpy as np
import pandas as pd
from sklearn.linear_model import LinearRegression
%pylab inline
import matplotlib.pyplot as plt

raw_data=pd.read_csv("Linear.csv")
raw_data.head(3)

filtered_data=raw_data[-np.isnan(raw_data["y"])]
filtered_data.head(3)

npMatrix=np.Matrix(filtered_data)
X,Y=npMatrix[:,0],npMatrix[:,1]
md1=LinearRegression.fit(X,Y)
m=md1.coef_[0]
c=md1.intercept
print("The required equation is y={0}x+{1}".format(m,c))


plt.scatter(X,Y,color='blue')
plt.plot([0,100],[c,m*100+c],'r')